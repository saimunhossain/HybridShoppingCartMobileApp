# HybridShoppingCartMobileApp

This project was generated with AngularJS

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Screenshots

![ScreenShot](/images/app.jpg)
![ScreenShot](/images/app2.jpg)

## Installation

```
git clone https://gitlab.com/saimunhossain/HybridShoppingCartMobileApp
cd HybridShoppingCartMobileApp
npm install
ionic serve
```